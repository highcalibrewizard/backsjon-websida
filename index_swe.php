<!DOCTYPE html>

<html lang="sv">

<head>
<meta charset="utf-8">
<title>Bäcksjön</title>
<link rel="stylesheet" href="css/style.css">
</head>

<body>

<?php
//CONNECTING
$dataBase = "backsjon";
$user = "user";
$password = "password";
$server = "localhost";

$connection = mysqli_connect($server,$user,$password,$dataBase);
mysqli_set_charset($connection,"utf8");

if(!$connection){
	die("Error connecting to server: ".mysqli_connect_error());
}
else{
	$sql = "SELECT * FROM news";
	$result = mysqli_query($connection,$sql);
	
	$newsArray = array();		//storing the news here

	if($result){

		if(mysqli_num_rows($result) > 0){
			while($row = mysqli_fetch_assoc($result)){
				$newsArray[] = $row;
			}
		}
	}
	
	$newsArray = array_reverse($newsArray);		//reverses the array, so that the most recent news post is on first index
			
}

?>
	
	<!-- This is for displaying images floating above -->
	<div Id="empty">
		<div id="imageShower"></div>
	</div>

	<div id="flagHolder">
		<a href="index_en.php"><img class="flag" src="img/britain.png" alt="English"/></a>
		<a href="index_swe.php"><img class="flag" src="img/sweden.png" alt="Svenska"/></a>
	</div>

	
	<div Id="introContainer" class="container">
		
		<!-- Title -->
		<div Id="titleContainer">
			<h1 Id="title">Bäcksjön</h1>
			<h2>Bad - Discgolf - Vandring - Fiske - Kulturlämningar</h2>
		</div>
		
		<!-- navigation -->
		
		<div Id="nav">
			<div class="linkCont"><a class="navLink" href="index_swe.php">Om</a></div>
			<div class="linkCont"><a class="navLink" href="history_swe.html">Historia</a></div>
			<div class="linkCont"><a class="navLink" href="activities_swe.html">Utflyktsmål</a></div>
			<div class="linkCont secondRow" id="lastLink"><a class="navLink" href="description_swe.html">Vägbeskrivning</a></div>
			<div class="linkCont secondRow"><a class="navLink" href="contact_swe.html">Kontakt</a></div>
		</div>	
	</div>
	
	
	<!-- Om/About/Sammanfattning -->
	<div id="contentAbout" class="container content">
	
		<img src="img/intro.jpg" alt="Bäcksjön" class="articleImage"/>
		<div class="textInfo" id="textInfoIntro">
			<h2 class="articleHeader">Bäcksjöns naturområde</h2>
<p>Bäcksjön är en mycket vacker insjö, belägen endast 15 km nordväst om Umeå. Närheten till staden gör att det är lätt att ta sig till området med cykel eller bil. Sjön är ca två km lång, en km bred och, som mest, sju meter djup. Miljön runt sjön är unik eftersom den helt saknar fritidsbebyggelse.</p> 

<p>Här finns, förutom chansen att uppleva ett fantastiskt fiske, även 
långgrunda badstränder, naturstigar, bär- och svampmarker. Flera rastplatser, med vindskydd och eldstäder, är utplacerade. Omkring sjön finns dessutom många intressanta kulturlämningar. Husgrunder, tjärdalar, kolbottnar m.m. vittnar om livet man levde vid sjön.</p>

<p>Bäcksjöns sportfiskeklubb ansvarar för fisket i Bäcksjön. Varje år sker utsättning av ädelfisk, t.ex. regnbåge, öring och harr.
Bäcksjön är en klarvattensjö med bra siktdjup och håller förutom ädelfisken goda bestånd av gädda, abborre, mört och lake.</p> 

<p>Vid kommunens provfiske konstaterades att de flesta och största matabborrarna fångades i Bäcksjön.
Ett populärt fiskeställe är strandremsan mellan Hemviken och Svartviken. Här finns grusbotten och en stor del av ädelfisken fångas här.</p>
		</div>
		
		
		<div class="contentDiv" id="newsSection">
		
		<?php
		//printing out news. the first news-post...
			if(count($newsArray) >0){
				print "<h2 class='articleHeader'>".$newsArray[0]["title"]."</h2>";
				print "<em><p>".$newsArray[0]["date"]."</p></em>";
				print "<p>".$newsArray[0]["text"]."</p>";
			}
			
			else{
				print "<h2 class='articleHeader'></h2>";
				print "<em><p></p></em>";
				print "<p></p>";
				
			}
		?>
			
			<div id="olderNewsDiv"><p><em>[+] äldre nyheter</em></p>
				<div id='oldNews'>	
				
				
				<?php
				//printing out the rest of the news
				for($i = 1; $i < count($newsArray); $i++){
					
					$current = $newsArray[$i];
					print "<div class='contentDiv oldNews'>";
					print "<h2 class='articleHeader'>".$current["title"]."</h2>";
					print "<em><p>".$current["date"]."</p></em>";
					print "<p>".$current["text"]."</p>";
					print "</div>";
					
					
					if($i == 5){		//Max 5 news items are displayed
						break;
					}
				}
				?>
				</div>
			</div>

	</div>
	</div>

	
	<!-- Footer -->
	<footer>
		<p>Site made by Benny Andersson, 2016. Historical text by Ulla-Karin Lindkvist. Photography by Erik Rahm<p>
	</footer>

</body>








<!-- JS -->
<script type="text/javascript" src="javascript/jquery-3.1.0.min.js"></script>
<script type="text/javascript" src="javascript/javascript.js"></script>
</html>