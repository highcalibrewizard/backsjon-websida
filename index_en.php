<!DOCTYPE html>

<html lang="en">

<head>
<meta charset="utf-8">
<title>Bäcksjön</title>
<link rel="stylesheet" href="css/style.css">
</head>

<body>

<?php
//CONNECTING
$dataBase = "backsjon";
$user = "user";
$password = "password";
$server = "localhost";

$connection = mysqli_connect($server,$user,$password,$dataBase);
mysqli_set_charset($connection,"utf8");

if(!$connection){
	die("Error connecting to server: ".mysqli_connect_error());
}
else{
	$sql = "SELECT * FROM news_eng";
	$result = mysqli_query($connection,$sql);
	
	$newsArray = array();		//storing the news here

	if($result){

		if(mysqli_num_rows($result) > 0){
			while($row = mysqli_fetch_assoc($result)){
				$newsArray[] = $row;
			}
		}
	}
	
	$newsArray = array_reverse($newsArray);		//reverses the array, so that the most recent news post is on first index
			
}

?>
	
	<!-- This is for displaying images floating above -->
	<div Id="empty">
		<div id="imageShower"></div>
	</div>

	<div id="flagHolder">
		<a href="index_en.php"><img class="flag" src="img/britain.png" alt="English"/></a>
		<a href="index_swe.php"><img class="flag" src="img/sweden.png" alt="Svenska"/></a>
	</div>

	
	<div Id="introContainer" class="container">
		
		<!-- Title -->
		<div Id="titleContainer">
			<h1 Id="title">Bäcksjön</h1>
			<h2>Swimming - Disc golf - Hiking - Fishing - Cultural remnants</h2>
		</div>
		
		<!-- navigation -->
		
		<div Id="nav">
			<div class="linkCont"><a class="navLink" href="index_en.php">About</a></div>
			<div class="linkCont"><a class="navLink" href="history_en.html">History</a></div>
			<div class="linkCont"><a class="navLink" href="activities_en.html">Activities</a></div>
			<div class="linkCont secondRow" id="lastLink"><a class="navLink" href="description_en.html">Directions</a></div>
			<div class="linkCont secondRow"><a class="navLink" href="contact_en.html">Contact</a></div>
		</div>	
	</div>
	
	
	<!-- Om/About/Sammanfattning -->
	<div id="contentAbout" class="container content">
	
		<img src="img/intro.jpg" alt="Bäcksjön" class="articleImage"/>
		<div class="textInfo" id="textInfoIntro">
<h2 class="articleHeader">Bäcksjön nature site</h2>

<p>Bäcksjön is a beautiful lake, situated only 15 km north west of Umeå. Being this close makes it easy to get to the area with bicycle or car. The lake is ca. 2 km long, 1 km wide and has a maximum depth of 7 m. The area around the lake is unique since it completely lacks settlement.</p>

<p>At the lake there are, besides amazing fishing, shallow beaches, trails, berries and mushrooms. There are several shelters for grilling. Around the lake there are also many cultural remnants. House ruins, tar pits and charcoal kilns bear witness of the past life around the lake.</p>

<p>Bäcksjön's Sport Fishing club is responsible for the fishing in the lake. Every year rainbow trouts, trouts and graylings are released. Besides these game fish there are also populations of pike, bass and burbot. Bäcksjön has clear water with good visibility.</p>

<p>It has been shown that Bäcksjön has the largest basses. A popular fishing site is the shore between Hemviken and Svartviken. The sea bottom at that site is made out of gravel. A large portion of the game fish is caught there.</p>
		</div>
		
		
		<div class="contentDiv" id="newsSection">
		
		<?php
		//printing out news. the first news-post...
			if(count($newsArray) >0){
				print "<h2 class='articleHeader'>".$newsArray[0]["title"]."</h2>";
				print "<em><p>".$newsArray[0]["date"]."</p></em>";
				print "<p>".$newsArray[0]["text"]."</p>";
			}
			
			else{
				print "<h2 class='articleHeader'></h2>";
				print "<em><p></p></em>";
				print "<p></p>";
				
			}
		?>
			
			<div id="olderNewsDiv"><p><em>[+] older news</em></p>
				<div id='oldNews'>	
				
				
				<?php
				//printing out the rest of the news
				for($i = 1; $i < count($newsArray); $i++){
					$current = $newsArray[$i];
					print "<div class='contentDiv oldNews'>";
					print "<h2 class='articleHeader'>".$current["title"]."</h2>";
					print "<em><p>".$current["date"]."</p></em>";
					print "<p>".$current["text"]."</p>";
					print "</div>";
					
					if($i == 5){		//Max 5 news items are displayed
						break;
					}
					
				}
				?>
				</div>
			</div>

	</div>
	</div>

	
	<!-- Footer -->
	<footer>
		<p>Site made by Benny Andersson, 2016. Historical text by Ulla-Karin Lindkvist. Photography by Erik Rahm<p>
	</footer>

</body>








<!-- JS -->
<script type="text/javascript" src="javascript/jquery-3.1.0.min.js"></script>
<script type="text/javascript" src="javascript/javascript.js"></script>
</html>