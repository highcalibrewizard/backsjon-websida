<?php
session_start();


/*
Cleaning up string data. do this on stuff that needs to be printed, like form data. otherwise you can inject javascript when the text is printed with php
*/
function cleanUp($stringData){
	$stringData = htmlspecialchars($stringData); //removes 
	$stringData = trim($stringData); //removed spaces
	$stringData = stripslashes($stringData);	//removes slashes
	return $stringData;
}

/*
Checks if string data contains illegal chars, returns false if so. should be used on usernames
*/
function checkIfContainsIllegals($stringData){
	if ($stringData != htmlspecialchars($stringData)){
		return true;
	}
	
	if($stringData != stripslashes($stringData)){
		return true;
	}
	
	return false;
}

function returnLang(){
	if ($_SESSION["lang"] == "swe"){
		return "news";
	}
	else if ($_SESSION["lang"] == "eng"){
		return "news_eng";
	}
}

$server = "localhost";
$user = cleanUp($_SESSION["user"]);
$password = cleanUp($_SESSION["pass"]);
$db = "backsjon";

$connection = mysqli_connect($server,$user,$password, $db);
mysqli_set_charset($connection, "utf8");


if (filter_input(INPUT_POST, "titleData") && filter_input(INPUT_POST, "dateData") && filter_input(INPUT_POST, "newsData")){	//if all data
	$titleData = trim(filter_input(INPUT_POST, "titleData"));
	$dateData = trim(filter_input(INPUT_POST, "dateData"));
	$newsData = trim(filter_input(INPUT_POST, "newsData"));
	
	if(!checkIfContainsIllegals($titleData) && !checkIfContainsIllegals($dateData) && !checkIfContainsIllegals($newsData)){
		
		if($connection){
			$sql = "INSERT INTO ". returnLang() ." (date, title, text) VALUES ('".$dateData."','".$titleData."','".$newsData."')";
			/*  mysqli_query($connection,$sql);*/
			if (mysqli_query($connection,$sql)){
				$data["message"] = "success";		//associative array, note that variable names are as strings
				print json_encode($data);					//encodes to json object. print will actually send the response
			}
			else{
				$data["message"] = "fail";
				$data["detail"] = "Database error";
				print json_encode($data);					//encodes to json object. print will actually send the response
			}

		}
		else{
			//$data = array("message"=>"fail", "detail"=>$user." id: ".session_id());		//associative array, note that variable names are as strings
			$data["message"] = "fail";
			$data["detail"] = "Connection refused";
			print json_encode($data);					//encodes to json object. print will actually send the response
		}		
	}
	else{
		//$titleData = filter_input(INPUT_POST, "titleData");		//this is how you receive data
		$data["message"] = "fail";
		$data["detail"] = "illegal characters. Html characters or slashes are not allowed";
		print json_encode($data);					//encodes to json object. print will actually send the response
	}
}
else{
	//$titleData = filter_input(INPUT_POST, "titleData");		//this is how you receive data
	$data["message"] = "fail";
	$data["detail"] = "Please fill all fields";
	print json_encode($data);					//encodes to json object. print will actually send the response
}



?>