<?php 
session_start(); 
?>


<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8">
<title>Admin-page for Bäcksjön</title>
<link rel="stylesheet" type="text/css" href="css/style_login.css">
<script type="text/javascript" src="javascript/jquery-3.1.0.min.js"></script>  <!-- use compressed production jquery 3.1.0 --->
<script type="text/javascript" src="javascript/loginScript.js"></script>
</head>

<body>
<?php


$lang = cleanUp(filter_input(INPUT_GET, "lang"));

if($lang){
	$_SESSION["lang"] = $lang;
	display();
}
else if($_SESSION["lang"]){
	$lang = $_SESSION["lang"];
	display();
}
else{
	print "<p>Log in <a href='newslogin.php'>here</a> instead</p>";
}

function display(){
global $lang;	
if($lang == "swe"){
	print "<p>You have selected Swedish news</p>";
}
else if($lang == "eng"){
	print "<p>You have selected English news</p>";
}
	
print <<<HERE
<div id="loginField">
<form action="login.php" method="post">
<fieldset>
<label>User name</label>
<input type="text" name="user"/>
<label>Password</label>
<input type="password" name="pass"/>
<button type="submit">Log in</button>
</fieldset>
</form>
</div>

<strong><p id="status">Server message:</p></strong>
<a href="newslogin.php">Back to language selection</a>
HERE;
}




//user:backsjon_admin <<remove this shit later
//pass: password


//Displaying post news section
function loggedIn($connection){
	
	$sql = "SELECT * FROM news";
	$result = mysqli_query($connection, $sql);
	
print <<<HERE
	<div id="newNewsField">
	<form id="postForm">
	<fieldset>
	<legend>Post news</legend>
	<label>Date(yyyy-mm-dd)</label>
	<input type="date" name="dateData">
	<label>Title</label>
	<input type="text" name="titleData">
	<label>News</label>
	<textarea rows="5" cols="50" name="newsData"></textarea>
	<button type="button" id="postNewsButton">submit</button
	</form>
	</fieldset>
	</div>
HERE;

print "<div id='newsContainer'>";
print "</div>";
print "<script>updateNews()</script>";
}

/*
Cleaning up string data. do this on stuff that needs to be printed, like form data. otherwise you can inject javascript when the text is printed with php
*/
function cleanUp($stringData){
	$stringData = strip_tags($stringData);	//removes angle brackets
	$stringData = htmlspecialchars($stringData); //removes html code
	$stringData = trim($stringData); //removed spaces
	$stringData = stripslashes($stringData);	//removes slashes
	return $stringData;
}

/*
Checks if string data contains illegal chars, returns false if so. should be used on usernames
*/
function checkIfContainsIllegals($stringData){
	if ($stringData != htmlspecialchars($stringData)){
		return true;
	}
	
	if($stringData != trim($stringData)){
		return true;
	}
	
	if($stringData != stripslashes($stringData)){
		return true;
	}
	
	return false;
}

//connecting
if (filter_input(INPUT_POST,"user") && filter_input(INPUT_POST,"pass")){
	
	$server = "localhost";
	$user = cleanUp(filter_input(INPUT_POST, "user"));
	$password = cleanUp(filter_input(INPUT_POST, "pass"));
	$db = "backsjon";
	$connection = mysqli_connect($server,$user,$password, $db);
	mysqli_set_charset($connection, "utf8");
	
	if ($connection){
		print "<p>Logged in as " . $user . "</p>";
		print "<script>hideNShow();</script>";		//showing the add news section
		
		$_SESSION["pass"] = $password;
		$_SESSION["user"] = $user;
		loggedIn($connection);
	}
	else{
		print "Error logging in: ".mysqli_connect_error();
	}
}





?>



</body>

</html>