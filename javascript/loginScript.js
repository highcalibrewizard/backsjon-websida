/*
shows news post form, hides login-form
*/
function hideNShow(){
	$("#newNewsField").show();
	$("#loginField").hide();
}

//updates news section
function updateNews(){

	$.ajax({
		url : "updateNews.php",
		dataType : "json"	
	})
	.done(function(resp){
	if(resp.message == "fail"){
		alert("failed updating news");
	}
	else{
		console.log("news updated: " + resp);
		
		printOutNews(resp);
	}
	})
	.fail(function(resp){
	alert("failed updating news");
	});
}


function printOutNews(dataToPrint){
	
	for(var i = 0; i < dataToPrint.length; i++){
		var htmlToAdd = "<div class='oldNews'>\
	<h2 class='title'>"+dataToPrint[i].title+"</h2>\
		<p class='date'>"+dataToPrint[i].date+"</p>\
		<p class='delete'>delete</p>\
		<p class='confirm'>Delete? Click on this link to confirm</p>\
		</div>";
		$("#newsContainer").append(htmlToAdd);
	}
	
	$(".oldNews:odd").css("background-color","silver");
	
	
	$(".delete").click(function(){		//when clicking delete show confirmation
		$(this).parent().children(".confirm").show();
	});
	
	$(".confirm").click(function(){
		
		var $title = $(this).parent().children(".title").html();
		var $date = $(this).parent().children(".date").html();
		var currentElement = $(this).parent();
		
		$.ajax({
			url: "delete.php",
			data: {
				"title" : $title,
				"date" : $date
			},
			type: "POST",
			dataType: "json"
		})
		.done(function(resp){			
			if(resp.status == "success"){
				alert(resp.message);
				$(currentElement).remove();		//deletes the element
			}
			else{
				alert("failure deleting this post. "+resp.message);
			}
		})
		.fail(function(resp){
			alert("failure deleting this post. "+resp.message);
		});
		
	});
	
}


$(document).ready(function(){
	
	$("#swedish").click(function(){
	window.location.href="login.php?lang=swe";
	});
	
	$("#english").click(function(){
	window.location.href="login.php?lang=eng";
	});
	
$("#postNewsButton").click(function(){
	
	$("#postNewsButton").toggle();  //hiding button
	var jsonArray = $("#postForm").serializeArray();	//serializes form data
	
	$("#status").html("attempting upload");
	
	$.ajax({
		url: "postNews.php",
		data: jsonArray,
		type: "POST",
		dataType: "json"
	})
	.done(function(resp){		//.always will always run, .done will run if successful
	
		if (resp.message == "fail"){
			$("#status").html(resp.message+" "+resp.detail);
			console.log(resp + " fail");
			$("#postNewsButton").toggle();  //hiding button
		}
		else{
			$("#status").html("Post uploaded successfully");
			console.log(resp + " success");
			$("#postNewsButton").toggle();  //hiding button
			$("#newsContainer").empty();	//removes old news, need to update it!
			updateNews();
		}
	
	})
	.fail(function(resp){
		$("#status").html(resp.message+" "+resp.detail);
		console.log(resp + " fail");
		$("#postNewsButton").toggle();  //hiding button
	});
});

});